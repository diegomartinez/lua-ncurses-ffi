
---
-- Bitwise operations.
--
-- This module tries to load several modules and returns the first one found.
-- Currently supported modules are:
--
-- * `ncurses.bit53`: Bundled module that uses [bitwise operators][bit53]
--   available in Lua 5.3.
-- * [`bit32`][bit32]: Native library included in Lua 5.2 and LuaJIT.
-- * [`bit`][bit]: Native library included in LuaJIT.
-- * [`bitty`][bitty]: Pure Lua implementation of bitwise operators.
--
-- The first module that succeeds in loading is used.
--
-- [bit53]: https://www.lua.org/manual/5.3/manual.html#3.4.2
-- [bit32]: https://luarocks.org/modules/siffiejoe/bit32
-- [bit]:   http://bitop.luajit.org/
-- [bitty]: https://gist.github.com/kaeza/8ee7e921c98951b4686d

local supported = {
	"ncurses.bit53",
	"bit32",
	"bit",
	"bitty",
}

local bit

for _, m in ipairs(supported) do
	local ok, lib = pcall(require, m)
	if ok and lib then
		lib._NAME = lib._NAME or m
		bit = lib
		break
	end
end

return assert(bit, "no supported bit library found")
