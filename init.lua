
---
-- FFI-based ncurses bindings.
--
-- The functions exported by this module match the C interface, except for
-- the following:
--
-- * Objects returned by functions that allocate memory and return it to the
--   caller (for example, `newwin`), set a `__gc` metamethod to the relevant
--   "free" function (e.g. `delwin`) so the objects are destroyed when they
--   are garbage collected.
-- * Output arguments are returned as extra values instead. In the case of
--   strings, buffers are allocated automatically as required.
-- * Functions that return integer error codes return true on success, or nil
--   failure.
-- * Potentially unsafe functions (for example, *getstr(3)*) call the saner
--   alternative (e.g. *getnstr(3)*) passing configurable parameters.
--
-- For example, the function:
--
--     int wgetnstr(WINDOW *w, char *buf, int size);
--
-- is exported by this module as:
--
--     wgetnstr(w, size) --> string
--
-- All functions listed here are wrappers. They serve to document in which way
-- they differ from the C functions. See [*ncurses(3)*][ncurses] for full
-- documentation. The "raw" versions can be accessed via the `lib` field.
--
-- [ncurses]: https://linux.die.net/man/3/ncurses
--
-- @module ncurses

local ncurses = { }

---
-- Attributes.
--
-- @table attributes
-- @tfield number value       Actual value.
-- @tfield string chartext    `value & A_CHARTEXT` as a 1-char string.
-- @tfield number charbyte    `value & A_CHARTEXT` as a number.
-- @tfield number color       `value & A_COLOR`.
-- @tfield boolean standout   True if `A_STANDOUT` is set.
-- @tfield boolean underline  True if `A_UNDERLINE` is set.
-- @tfield boolean reverse    True if `A_REVERSE` is set.
-- @tfield boolean blink      True if `A_BLINK` is set.
-- @tfield boolean dim        True if `A_DIM` is set.
-- @tfield boolean bold       True if `A_BOLD` is set.
-- @tfield boolean altcharset True if `A_ALTCHARSET` is set.
-- @tfield boolean invis      True if `A_INVIS` is set.
-- @tfield boolean protect    True if `A_PROTECT` is set.
-- @tfield boolean horizontal True if `A_HORIZONTAL` is set.
-- @tfield boolean left       True if `A_LEFT` is set.
-- @tfield boolean low        True if `A_LOW` is set.
-- @tfield boolean right      True if `A_RIGHT` is set.
-- @tfield boolean top        True if `A_TOP` is set.
-- @tfield boolean vertical   True if `A_VERTICAL` is set.
-- @tfield boolean italic     True if `A_ITALIC` is set.

---
-- Default buffer size for `getstr` and similar functions.
-- Default is 40.
--
ncurses.GETSTRSIZE = 40

local ffi = require "ffi"
--~ local bit = require "ncurses.bit"

ffi.cdef(require "ncurses.cdef")

ncurses.lib = ffi.load("ncurses")

---
-- `COLOR_xxx` constants. Values are numbers.
-- Intended usage is `COLOR.xxx`.
--
ncurses.COLOR = require "ncurses.defines.color"

---
-- `ACS_xxx` constants. Values are numbers.
-- Intended usage is `ACS.xxx`.
--
ncurses.ACS = require "ncurses.defines.acs"

---
-- `KEY_xxx` constants. Values are strings.
-- Intended usage is `KEY.xxx`.
--
ncurses.KEY = require "ncurses.defines.key"

---
-- `A_xxx` (attributes) constants. Values are numbers.
-- Intended usage is `A.xxx`.
--
ncurses.A = require "ncurses.defines.a"

---
-- TODO: Doc.
--
ncurses.ERR = -1

---
-- TODO: Doc.
--
ncurses.OK = 0

local function OK(r)
	return r ~= ncurses.ERR
end

local function pack(...)
	return { n=select("#", ...), ... }
end

---
-- Wrapper to reset the terminal in case of errors.
--
-- In order to not leave the terminal in the wrong state, we call a function
-- in a protected context, so we can catch the error, restore the terminal
-- to its original state, then call the error handler (if any).
--
-- @tparam function func Function to run.
-- @tparam function|nil errfunc Error handler.
-- @tparam any ... Function arguments.
-- @treturn boolean True on success, false on error.
-- @usage
-- local function main()
--   -- Your code here.
--   -- Initialization and deinitialization are done automatically.
-- end
-- ncurses.xpcall(main, errfunc, ...)
function ncurses.xpcall(func, errfunc, ...)
	local ret, err
	local args = pack(...)
	ncurses.initscr()
	xpcall(function()
		ret = pack(func(unpack(args, 1, args.n)))
	end, function(e)
		ret, err = nil, e
		ncurses.endwin()
		if errfunc then
			errfunc(e)
		end
	end)
	ncurses.endwin()
	if not ret then
		return nil, err
	end
	return true, unpack(ret, 1, ret.n)
end

---
-- Wrapper to reset the terminal in case of errors.
--
-- Equivalent to `ncurses.xpcall(func, nil, ...)`.
--
-- @tparam function func Function to run.
-- @tparam any ... Function arguments.
-- @usage
-- local function main()
--   -- Your code here.
--   -- Initialization and deinitialization are done automatically.
-- end
-- ncurses.pcall(main, ...)
function ncurses.pcall(func, ...)
	return ncurses.xpcall(func, nil, ...)
end

---
-- Wrapper to reset the terminal in case of errors.
--
-- Equivalent to `ncurses.xpcall(func, ncurses.errhand, ...)`.
--
-- @tparam function func Function to run.
-- @tparam any ... Function arguments.
-- @usage
-- local function main()
--   -- Your code here.
--   -- Initialization and deinitialization are done automatically.
-- end
-- ncurses.call(main, ...)
function ncurses.call(func, ...)
	return ncurses.xpcall(func, ncurses.errhand, ...)
end

---
-- Error handler.
--
-- Prints `err` and a traceback to stderr, then exits with status code -1.
-- This function is useful as the `errfunc` parameter to `ncurses.xpcall`.
--
-- @tparam string err Error message.
function ncurses.errhand(err)
	io.stderr:write(tostring(err), "\n", debug.traceback(), "\n")
	os.exit(-1)
end

---
-- Sets *delwin(3)* as `__gc` metamethod for the window.
--
-- @tparam WINDOW win
-- @treturn WINDOW Window.
function ncurses.gcwindow(win)
	if ffi.cast("int", win) == 0 then return nil end
	return ffi.gc(win, ncurses.lib.delwin)
end

---
-- Calls *newwin(3)* and sets *delwin(3)* as `__gc` metamethod for the window.
--
-- @tparam number nlines
-- @tparam number ncols
-- @tparam number begin_y
-- @tparam number begin_x
-- @treturn WINDOW
function ncurses.newwin(nlines, ncols, begin_y, begin_x)
	return ncurses.gcwindow(
			ncurses.lib.newwin(nlines, ncols, begin_y, begin_x))
end

---
-- Calls *subwin(3)* and sets *delwin(3)* as `__gc` metamethod for the window.
--
-- @tparam WINDOW orig
-- @tparam number nlines
-- @tparam number ncols
-- @tparam number begin_y
-- @tparam number begin_x
-- @treturn WINDOW
function ncurses.subwin(orig, nlines, ncols, begin_y, begin_x)
	return ncurses.gcwindow(
			ncurses.lib.subwin(orig, nlines, ncols, begin_y, begin_x))
end

---
-- Calls *derwin(3)* and sets *delwin(3)* as `__gc` metamethod for the window.
--
-- @tparam WINDOW orig
-- @tparam number nlines
-- @tparam number ncols
-- @tparam number par_y
-- @tparam number par_x
-- @treturn WINDOW
function ncurses.derwin(orig, nlines, ncols, par_y, par_x)
	return ncurses.gcwindow(
			ncurses.lib.derwin(orig, nlines, ncols, par_y, par_x))
end

---
-- Calls *dupwin(3)* and sets *delwin(3)* as `__gc` metamethod for the window.
--
-- @tparam WINDOW orig
-- @treturn WINDOW
function ncurses.dupwin(orig)
	return ncurses.gcwindow(ncurses.lib.dupwin(orig))
end

---
-- No-op.
--
-- Windows are automatically garbage-collected.
--
-- @tparam WINDOW win
function ncurses.delwin(win) -- luacheck: ignore
end

---
-- Calls *newpad(3)* and sets *delwin(3)* as `__gc` metamethod for the pad.
--
-- @tparam number nlines
-- @tparam number ncols
-- @treturn WINDOW
function ncurses.newpad(nlines, ncols)
	return ncurses.gcwindow(ncurses.lib.newpad(nlines, ncols))
end

---
-- Calls *subpad(3)* and sets *delwin(3)* as `__gc` metamethod for the pad.
--
-- @tparam WINDOW orig
-- @tparam number nlines
-- @tparam number ncols
-- @tparam number begin_y
-- @tparam number begin_x
-- @treturn WINDOW
function ncurses.subpad(orig, nlines, ncols, begin_y, begin_x)
	return ncurses.gcwindow(
			ncurses.lib.subpad(orig, nlines, ncols, begin_y, begin_x))
end

---
-- Calls *getch(3)*.
--
-- @treturn string
--  On success, returns a string. In the case of pseudo-keys, the returned
--  string has length 2; otherwise it has length 1. Returns nil if the
--  underlying call returns `ERR` (-1).
function ncurses.getch()
	return ncurses.KEY._K(ncurses.lib.getch())
end

---
-- Calls *wgetch(3)*.
--
-- @tparam WINDOW win
-- @treturn string
--  On success, returns a string. In the case of pseudo-keys, the returned
--  string has length 2; otherwise it has length 1. Returns nil if the
--  underlying call returns `ERR` (-1).
function ncurses.wgetch(win)
	return ncurses.KEY._K(ncurses.lib.wgetch(win))
end

---
-- Calls *inch(3)*.
--
-- @treturn string Character.
-- @treturn attributes Attributes.
function ncurses.inch()
	return ncurses.A._split(ncurses.lib.inch())
end

---
-- Calls *winch(3)*.
--
-- @tparam WINDOW win
-- @treturn attributes Attributes.
function ncurses.winch(win)
	return ncurses.A._split(ncurses.lib.winch(win))
end

---
-- Calls `getnstr` with `GETSTRSIZE` as argument.
--
-- @treturn string
function ncurses.getstr()
	return ncurses.getnstr(ncurses.GETSTRSIZE)
end

---
-- Allocates a buffer of the specified size and calls *getnstr(3)*.
--
-- @tparam number size
-- @treturn string
function ncurses.getnstr(size)
	local buf = ffi.new("char[?]", size)
	if not OK(ncurses.lib.getnstr(buf, size)) then
		return nil
	end
	return ffi.string(buf)
end

---
-- Calls `wgetnstr` with `GETSTRSIZE` as argument.
--
-- @tparam WINDOW w
-- @treturn string
function ncurses.wgetstr(w)
	return ncurses.wgetnstr(w, ncurses.GETSTRSIZE)
end

---
-- Allocates a buffer of the specified size and calls *wgetnstr(3)*.
--
-- @tparam WINDOW win
-- @tparam number size
-- @treturn string
function ncurses.wgetnstr(win, size)
	local buf = ffi.new("char[?]", size+1)
	local r = ncurses.lib.wgetnstr(win, buf, size)
	if not OK(r) then return nil end
	return ffi.string(buf)
end

---
-- TODO: Doc.
--
function ncurses.wgetbkgd(win)
	return ncurses.A._split(ncurses.lib.wgetbkgd(win))
end

---
-- TODO: Doc.
--
function ncurses.wbkgdset(win, ...)
	return ncurses.lib.wbkgdset(win, ncurses.A._join(...))
end

---
-- TODO: Doc.
--
function ncurses.wbkgd(win, ...)
	return OK(ncurses.lib.wbkgd(win, ncurses.A._join(...)))
end

local function adddefs(m)
	m = require(m)
	m.lib = ncurses.lib
	for k, v in pairs(m) do
		ncurses[k] = v
	end
end

adddefs "ncurses.defines.macros"

return setmetatable(ncurses, {
	__index = function(_, k)
		return ncurses.lib[k]
	end,
})
