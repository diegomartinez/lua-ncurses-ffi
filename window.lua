
---
-- Object-oriented approach to ncurses `WINDOW` objects.
--
-- Note: Thanks to metatables, calling the module is the same as calling the
-- `sub` function, thus the following code snippets are all equivalent:
--
--     w = window(...)
--     w = window.sub(somewindow, ...)
--     w = window.sub(nil, ...)
--     w = window:sub(...)
--
-- @classmod ncurses.window

local ncurses = require "ncurses"
local utils = require "ncurses.utils"

local function OK(x)
	return x ~= ncurses.ERR or nil
end

local window = { }
window.__index = window

---
-- Userdata containing a raw pointer to the underlying ncurses `WINDOW` object.
--
window.handle = nil

local function attach(handle)
	return setmetatable({ handle=handle }, window)
end

---
-- Create a new (sub-)window.
--
-- It may be used as `window.new(WINDOW, ...)` (where `WINDOW` is an userdata
-- object representing an ncurses *WINDOW(3)* object),
-- `window.new(winobj, ...)`, or `winobj:new(...)` (where `winobj` is another
-- window object created by this module.
--
-- This function uses *derwin(3)* internally.
--
-- @tparam ?number nlines Number of lines. If 0 or nil, `$LINES` is assumed.
-- @tparam ?number ncols Number of columns. If 0 or nil, `$COLUMNS` is assumed.
-- @tparam ?number start_y Vertical offset in the parent. Default is 0.
-- @tparam ?number start_x Horizontal offset in the parent. Default is 0.
-- @treturn window|nil
--  The window object on success, nil on error.
function window:new(nlines, ncols, start_y, start_x)
	nlines, ncols = nlines or 0, ncols or 0
	start_y, start_x = start_y or 0, start_x or 0
	local parent = window.type(self) and self.handle or ncurses.stdscr
	local handle = ncurses.derwin(parent, nlines, ncols, start_y, start_x)
	return attach(assert(handle, "derwin(3) failed"))
end

---
-- Alias for `new`.
--
function window:sub(...)
	return self:new(...)
end

---
-- Tests if the object is a window created by this module.
--
-- Intended usage is `window.type(x)`. Indended to mimic `io.type`.
--
-- @treturn string|nil
--  If the object is created by this module, returns `"window"` if its handle
--  is valid, or `"destroyed window"` if it is invalid; otherwise returns nil.
function window:type()
	if getmetatable(self) == window then
		return self.handle and "window" or "destroyed window"
	end
	return nil
end

---
-- Move the window.
--
-- This function uses *mvderwin(3)* internally.
--
-- @tparam number row Vertical offset in the parent.
-- @tparam number col Horizontal offset in the parent.
function window:mv(row, col)
	return OK(ncurses.mvderwin(self.handle or ncurses.stdscr, row, col))
end

---
-- TODO: Doc.
--
function window:getmaxx()
	return ncurses.getmaxx(self.handle or ncurses.stdscr)
end

---
-- TODO: Doc.
--
function window:getmaxy()
	return ncurses.getmaxy(self.handle or ncurses.stdscr)
end

---
-- Add a string to this window.
--
-- @tparam any ... Values to output. They are converted with `tostring` first.
-- @treturn boolean
--  True on success, nil on error.
function window:addstr(...)
	return OK(ncurses.waddstr(self.handle or ncurses.stdscr,
			utils.concat(...)))
end

---
-- Move the cursor and add a string to this window.
--
-- @tparam number row Row where to place the string.
-- @tparam number col Column where to place the string.
-- @tparam any ... Values to output. They are converted with `tostring` first.
-- @treturn boolean
--  True on success, nil on error.
function window:mvaddstr(row, col, ...)
	return OK(ncurses.mvwaddstr(self.handle or ncurses.stdscr,
			row, col, utils.concat(...)))
end

---
-- Add a string to this window.
--
-- @tparam string fmt `printf`-like format.
-- @tparam any ... Format arguments.
-- @treturn boolean
--  True on success, nil on error.
function window:addstrf(fmt, ...)
	return OK(ncurses.waddstr(self.handle or ncurses.stdscr,
			fmt:format(...)))
end

---
-- Move the cursor and add a string to this window.
--
-- @tparam number row Row where to place the string.
-- @tparam number col Column where to place the string.
-- @tparam string fmt `printf`-like format.
-- @tparam any ... Format arguments.
-- @treturn boolean
--  True on success, nil on error.
function window:mvaddstrf(row, col, fmt, ...)
	return OK(ncurses.mvwaddstr(self.handle or ncurses.stdscr,
			row, col, fmt:format(...)))
end

---
-- Refresh the screen with the contents of this window.
--
-- @tparam ?boolean nout If true, uses *wnoutrefresh(3)*;
--  otherwise uses *wrefresh(3)*. Default is false
-- @treturn boolean
--  True on success, nil on error.
function window:refresh(nout)
	local f = nout and ncurses.wnoutrefresh or ncurses.wrefresh
	return OK(f(self.handle or ncurses.stdscr))
end

---
-- Clear this window's contents.
--
-- @treturn boolean
--  True on success, nil on error.
function window:clear()
	return ncurses.wclear(self.handle or ncurses.stdscr)
end

---
-- TODO: Doc.
--
function window:erase()
	return ncurses.werase(self.handle or ncurses.stdscr)
end

---
-- TODO: Doc.
--
function window:clrtoeol()
	return OK(ncurses.wclrtoeol(self.handle or ncurses.stdscr))
end

---
-- TODO: Doc.
--
function window:clrtobot()
	return OK(ncurses.wclrtobot(self.handle or ncurses.stdscr))
end

---
-- TODO: Doc.
--
function window:getbkgd()
	return ncurses.getbkgd(self.handle or ncurses.stdscr)
end

---
-- TODO: Doc.
--
function window:bkgdset(...)
	return ncurses.wbkgdset(self.handle or ncurses.stdscr, ...)
end

---
-- TODO: Doc.
--
function window:bkgd(...)
	return ncurses.wbkgd(self.handle or ncurses.stdscr, ...)
end

---
-- Duplicate this window.
--
-- @treturn window|nil
--  New window object on success, nil on error.
function window:dup()
	local dup = ncurses.dupwin(self.handle or ncurses.stdscr)
	if not dup then return nil end
	return attach(dup)
end

---
-- Read a character from this window.
--
-- @treturn string|nil
--  String on success, nil on error.
function window:getch()
	return ncurses.wgetch(self.handle or ncurses.stdscr)
end

---
-- TODO: Doc.
--
function window:inch()
	return ncurses.winch(self.handle or ncurses.stdscr)
end

---
-- TODO: Doc.
--
function window:mvinch(y, x)
	return ncurses.mvwinch(self.handle or ncurses.stdscr, y, x)
end

---
-- Read a string from the user on this window.
--
-- @tparam ?number n Max number of characters to read.
--  Default is `ncurses.GETSTRSIZE`.
-- @treturn string|nil
--  String on success, nil on error.
function window:getstr(n)
	local r = ncurses.wgetnstr(self.handle or ncurses.stdscr,
			n or ncurses.GETSTRSIZE)
	return r
end

local function CH(x)
	return type(x) == "string" and x:byte() or x
end

---
-- TODO: Doc.
--
-- @tparam ?string|number ls Left border.
-- @tparam ?string|number rs Right border.
-- @tparam ?string|number ts Top border.
-- @tparam ?string|number bs Bottom border.
-- @tparam ?string|number tl Top left corner.
-- @tparam ?string|number tr Top right corner.
-- @tparam ?string|number bl Bottom left corner.
-- @tparam ?string|number br Bottom right corner.
-- @treturn boolean
--  True on success, nil on error.
function window:border(ls, rs, ts, bs, tl, tr, bl, br)
	ls, rs = CH(ls or ncurses.ACS.VLINE), CH(rs or ncurses.ACS.VLINE)
	ts, bs = CH(ts or ncurses.ACS.HLINE), CH(bs or ncurses.ACS.HLINE)
	tl, tr = CH(tl or ncurses.ACS.ULCORNER), CH(tr or ncurses.ACS.URCORNER)
	bl, br = CH(bl or ncurses.ACS.LLCORNER), CH(br or ncurses.ACS.LRCORNER)
	return OK(ncurses.wborder(self.handle or ncurses.stdscr,
			ls, rs, ts, bs, tl, tr, bl, br))
end

---
-- Move the cursor.
--
-- @tparam number row Row.
-- @tparam number col Column.
-- @treturn boolean
--  True on success, nil on error.
function window:move(row, col)
	return OK(ncurses.wmove(self.handle or ncurses.stdscr, row, col))
end

---
-- Turn on attributes.
--
-- @tparam string|number|ncurses.attributes ... Attribute(s).
-- @treturn boolean
--  True on success, nil on error.
function window:attron(...)
	return OK(ncurses.wattron(self.handle or ncurses.stdscr, ...))
end

---
-- Turn off attributes.
--
-- @tparam string|number|ncurses.attributes ... Attribute(s).
-- @treturn boolean
--  True on success, nil on error.
function window:attroff(...)
	return OK(ncurses.wattroff(self.handle, ...))
end

---
-- Helper method to turn on color.
--
-- @tparam number n Color pair.
-- @treturn boolean
--  True on success, nil on error.
function window:pairon(n)
	return self:attron(ncurses.COLOR_PAIR(n))
end

---
-- Helper method to turn off color.
--
-- @tparam number n Color pair.
-- @treturn boolean
--  True on success, nil on error.
function window:pairoff(n)
	return self:attroff(ncurses.COLOR_PAIR(n))
end

---
-- TODO: Doc.
--
function window:hline(ch, n)
	return OK(ncurses.whline(self.handle or ncurses.stdscr, ch, n))
end

---
-- TODO: Doc.
--
function window:vline(ch, n)
	return OK(ncurses.wvline(self.handle or ncurses.stdscr, ch, n))
end

---
-- TODO: Doc.
--
function window:keypad(flag)
	return OK(ncurses.keypad(self.handle or ncurses.stdscr, flag))
end

---
-- TODO: Doc.
--
function window:timeout(ms)
	return OK(ncurses.wtimeout(self.handle or ncurses.stdscr, ms))
end

---
-- TODO: Doc.
--
function window:scrollok(ms)
	return OK(ncurses.wtimeout(self.handle or ncurses.stdscr, ms))
end

---
-- TODO: Doc.
--
function window:tostring()
	if not window.type(self) then
		return tostring(self)
	end
	return ("<ncurses.window: %s>"):format(self.handle or "stdscr")
end

---
-- Alias for `tostring`.
--
function window:__tostring(...)
	return self:tostring(...)
end

---
-- Checks if two windows refer to the same underlying handle.
--
-- @tparam window other Other window.
-- @treturn boolean
--  True if the windows have the same underlying handle, false otherwise.
--  Returns nil if either window has a nil handle.
function window:equals(other)
	local h1 = window.type(self) and self.handle or nil
	local h2 = window.type(other) and other.handle or nil
	if not (h1 and h2) then return false end
	return h1 == h2
end

---
-- Alias for `equals`.
--
function window:__eq(...)
	return self:equals(...)
end

return attach()
