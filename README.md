
# lua-ncurses-ffi

LuaJIT FFI-based ncurses bindings.

## License

This library is free software released under a MIT-like license.
See `LICENSE.md` for details.

The file `cdef.lua` is based on `ncurses.h` and carries its own
license notice.

## Requirements

* [LuaJIT][luajit] (tested on 2.0.4).

[luajit]: http://luajit.org/
