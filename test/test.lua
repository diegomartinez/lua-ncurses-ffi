
local ncurses = require "ncurses"

ncurses.call(function()
	ncurses.addstr("Hello, world!\n")
	ncurses.addstr("What's your name?\n")
	local name = ncurses.getstr()
	if name and #name>0 then
		ncurses.addstr("Hello, "..name.."!\n")
	end
	ncurses.addstr("Press any key to quit...\n")
	ncurses.noecho()
	ncurses.getch()
	ncurses.echo()
end)
