
local M = { }

function M.scroll(win)
	return M.lib.wscrl(win, 1)
end

function M.touchwin(win)
	return M.lib.wtouchln(win, 0, M.lib.getmaxy(win), 1)
end

function M.touchline(win, s, c)
	return M.lib.wtouchln(win, s, c, 1)
end

function M.untouchwin(win)
	return M.lib.wtouchln(win, 0, M.lib.getmaxy(win), 0)
end

function M.box(win, v, h)
	return M.lib.wborder(win, v, v, h, h, 0, 0, 0, 0)
end

function M.border(ls, rs, ts, bs, tl, tr, bl, br)
	return M.lib.wborder(M.lib.stdscr, ls, rs, ts, bs, tl, tr, bl, br)
end

function M.hline(ch, n)
	return M.lib.whline(M.lib.stdscr, ch, n)
end

function M.vline(ch, n)
	return M.lib.wvline(M.lib.stdscr, ch, n)
end

function M.winstr(w, s)
	return M.lib.winnstr(w, s, -1)
end

function M.winchstr(w, s)
	return M.lib.winchnstr(w, s, -1)
end

function M.winsstr(w, s)
	return M.lib.winsnstr(w, s, -1)
end

return M
