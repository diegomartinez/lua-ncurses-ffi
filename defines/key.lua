
local function K(x)
	if x == -1 then return nil end
	if x < 0x100 then
		return string.char(x)
	else
		return string.char(x%0x100, math.floor(x/0x100)%0x100)
	end
end

-- Dirty hack to specify octal numbers.
local function O(x)
	return tonumber(tostring(x), 8)
end

return {
	_K          = K,          -- For internal use by `ncurses`.
	CODE_YES    = K(O(0400)), --/* A wchar_t contains a key code */
	MIN         = K(O(0401)), --/* Minimum curses key */
	BREAK       = K(O(0401)), --/* Break key (unreliable) */
	SRESET      = K(O(0530)), --/* Soft (partial) reset (unreliable) */
	RESET       = K(O(0531)), --/* Reset or hard reset (unreliable) */
	DOWN        = K(O(0402)), --/* down-arrow key */
	UP          = K(O(0403)), --/* up-arrow key */
	LEFT        = K(O(0404)), --/* left-arrow key */
	RIGHT       = K(O(0405)), --/* right-arrow key */
	HOME        = K(O(0406)), --/* home key */
	BACKSPACE   = K(O(0407)), --/* backspace key */
	F0          = K(O(0410)), --/* Function keys.  Space for 64 */
	DL          = K(O(0510)), --/* delete-line key */
	IL          = K(O(0511)), --/* insert-line key */
	DC          = K(O(0512)), --/* delete-character key */
	IC          = K(O(0513)), --/* insert-character key */
	EIC         = K(O(0514)), --/* sent by rmir or smir in insert mode */
	CLEAR       = K(O(0515)), --/* clear-screen or erase key */
	EOS         = K(O(0516)), --/* clear-to-end-of-screen key */
	EOL         = K(O(0517)), --/* clear-to-end-of-line key */
	SF          = K(O(0520)), --/* scroll-forward key */
	SR          = K(O(0521)), --/* scroll-backward key */
	NPAGE       = K(O(0522)), --/* next-page key */
	PPAGE       = K(O(0523)), --/* previous-page key */
	STAB        = K(O(0524)), --/* set-tab key */
	CTAB        = K(O(0525)), --/* clear-tab key */
	CATAB       = K(O(0526)), --/* clear-all-tabs key */
	ENTER       = K(O(0527)), --/* enter/send key */
	PRINT       = K(O(0532)), --/* print key */
	LL          = K(O(0533)), --/* lower-left key (home down) */
	A1          = K(O(0534)), --/* upper left of keypad */
	A3          = K(O(0535)), --/* upper right of keypad */
	B2          = K(O(0536)), --/* center of keypad */
	C1          = K(O(0537)), --/* lower left of keypad */
	C3          = K(O(0540)), --/* lower right of keypad */
	BTAB        = K(O(0541)), --/* back-tab key */
	BEG         = K(O(0542)), --/* begin key */
	CANCEL      = K(O(0543)), --/* cancel key */
	CLOSE       = K(O(0544)), --/* close key */
	COMMAND     = K(O(0545)), --/* command key */
	COPY        = K(O(0546)), --/* copy key */
	CREATE      = K(O(0547)), --/* create key */
	END         = K(O(0550)), --/* end key */
	EXIT        = K(O(0551)), --/* exit key */
	FIND        = K(O(0552)), --/* find key */
	HELP        = K(O(0553)), --/* help key */
	MARK        = K(O(0554)), --/* mark key */
	MESSAGE     = K(O(0555)), --/* message key */
	MOVE        = K(O(0556)), --/* move key */
	NEXT        = K(O(0557)), --/* next key */
	OPEN        = K(O(0560)), --/* open key */
	OPTIONS     = K(O(0561)), --/* options key */
	PREVIOUS    = K(O(0562)), --/* previous key */
	REDO        = K(O(0563)), --/* redo key */
	REFERENCE   = K(O(0564)), --/* reference key */
	REFRESH     = K(O(0565)), --/* refresh key */
	REPLACE     = K(O(0566)), --/* replace key */
	RESTART     = K(O(0567)), --/* restart key */
	RESUME      = K(O(0570)), --/* resume key */
	SAVE        = K(O(0571)), --/* save key */
	SBEG        = K(O(0572)), --/* shifted begin key */
	SCANCEL     = K(O(0573)), --/* shifted cancel key */
	SCOMMAND    = K(O(0574)), --/* shifted command key */
	SCOPY       = K(O(0575)), --/* shifted copy key */
	SCREATE     = K(O(0576)), --/* shifted create key */
	SDC         = K(O(0577)), --/* shifted delete-character key */
	SDL         = K(O(0600)), --/* shifted delete-line key */
	SELECT      = K(O(0601)), --/* select key */
	SEND        = K(O(0602)), --/* shifted end key */
	SEOL        = K(O(0603)), --/* shifted clear-to-end-of-line key */
	SEXIT       = K(O(0604)), --/* shifted exit key */
	SFIND       = K(O(0605)), --/* shifted find key */
	SHELP       = K(O(0606)), --/* shifted help key */
	SHOME       = K(O(0607)), --/* shifted home key */
	SIC         = K(O(0610)), --/* shifted insert-character key */
	SLEFT       = K(O(0611)), --/* shifted left-arrow key */
	SMESSAGE    = K(O(0612)), --/* shifted message key */
	SMOVE       = K(O(0613)), --/* shifted move key */
	SNEXT       = K(O(0614)), --/* shifted next key */
	SOPTIONS    = K(O(0615)), --/* shifted options key */
	SPREVIOUS   = K(O(0616)), --/* shifted previous key */
	SPRINT      = K(O(0617)), --/* shifted print key */
	SREDO       = K(O(0620)), --/* shifted redo key */
	SREPLACE    = K(O(0621)), --/* shifted replace key */
	SRIGHT      = K(O(0622)), --/* shifted right-arrow key */
	SRSUME      = K(O(0623)), --/* shifted resume key */
	SSAVE       = K(O(0624)), --/* shifted save key */
	SSUSPEND    = K(O(0625)), --/* shifted suspend key */
	SUNDO       = K(O(0626)), --/* shifted undo key */
	SUSPEND     = K(O(0627)), --/* suspend key */
	UNDO        = K(O(0630)), --/* undo key */
	MOUSE       = K(O(0631)), --/* Mouse event has occurred */
	RESIZE      = K(O(0632)), --/* Terminal resize event */
	EVENT       = K(O(0633)), --/* We were interrupted by an event */
	MAX         = K(O(0777)), --/* Maximum key value is 0633 */
}
