
local B = string.byte
local ACS = {
	ULCORNER = B'l', --/* upper left corner */
	LLCORNER = B'm', --/* lower left corner */
	URCORNER = B'k', --/* upper right corner */
	LRCORNER = B'j', --/* lower right corner */
	LTEE     = B't', --/* tee pointing right */
	RTEE     = B'u', --/* tee pointing left */
	BTEE     = B'v', --/* tee pointing up */
	TTEE     = B'w', --/* tee pointing down */
	HLINE    = B'q', --/* horizontal line */
	VLINE    = B'x', --/* vertical line */
	PLUS     = B'n', --/* large plus or crossover */
	S1       = B'o', --/* scan line 1 */
	S9       = B's', --/* scan line 9 */
	DIAMOND  = B'`', --/* diamond */
	CKBOARD  = B'a', --/* checker board (stipple) */
	DEGREE   = B'f', --/* degree symbol */
	PLMINUS  = B'g', --/* plus/minus */
	BULLET   = B'~', --/* bullet */
	LARROW   = B',', --/* arrow pointing left */
	RARROW   = B'+', --/* arrow pointing right */
	DARROW   = B'.', --/* arrow pointing down */
	UARROW   = B'-', --/* arrow pointing up */
	BOARD    = B'h', --/* board of squares */
	LANTERN  = B'i', --/* lantern symbol */
	BLOCK    = B'0', --/* solid square block */
	S3       = B'p', --/* scan line 3 */
	S7       = B'r', --/* scan line 7 */
	LEQUAL   = B'y', --/* less/equal */
	GEQUAL   = B'z', --/* greater/equal */
	PI       = B'{', --/* Pi */
	NEQUAL   = B'|', --/* not equal */
	STERLING = B'}', --/* UK pound sign */
}

ACS.BSSB = ACS.ULCORNER
ACS.SSBB = ACS.LLCORNER
ACS.BBSS = ACS.URCORNER
ACS.SBBS = ACS.LRCORNER
ACS.SBSS = ACS.RTEE
ACS.SSSB = ACS.LTEE
ACS.SSBS = ACS.BTEE
ACS.BSSS = ACS.TTEE
ACS.BSBS = ACS.HLINE
ACS.SBSB = ACS.VLINE
ACS.SSSS = ACS.PLUS

local lib

return setmetatable({ }, {
	__index = function(_, k)
		lib = lib or require "ncurses".lib
		local v = ACS[k]
		return v and lib.acs_map[v]
	end,
})
