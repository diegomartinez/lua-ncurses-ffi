
local bit = require "ncurses.bit"

local function isset(n, b)
	return math.floor((n/ (2^b)))%2 == 1 or nil
end

local function split(n)
	return {
		value      = n,
		chartext   = string.char(n%0x100),
		charbyte   = n%0x100,
		color      = math.floor(n/0x100)%0x100,
		standout   = isset(n, 16),
		underline  = isset(n, 17),
		reverse    = isset(n, 18),
		blink      = isset(n, 19),
		dim        = isset(n, 20),
		bold       = isset(n, 21),
		altcharset = isset(n, 22),
		invis      = isset(n, 23),
		protect    = isset(n, 24),
		horizontal = isset(n, 25),
		left       = isset(n, 26),
		low        = isset(n, 27),
		right      = isset(n, 28),
		top        = isset(n, 29),
		vertical   = isset(n, 30),
		italic     = isset(n, 31),
	}
end

local function join(...)
	local n, t = select("#", ...), { ... }
	local value = 0
	for i = 1, n do
		local a = t[i]
		local v
		if type(a) == "number" then
			v = a
		elseif type(a) == "string" then
			v = a:byte()
		elseif type(a) == "table" then
			local ch = a.charbyte or (a.chartext and a.chartext:byte()) or 0
			v = (ch
					+math.floor(a.color%0x100)
					+(a.standout   and (2^16) or 0)
					+(a.underline  and (2^17) or 0)
					+(a.reverse    and (2^18) or 0)
					+(a.blink      and (2^19) or 0)
					+(a.dim        and (2^20) or 0)
					+(a.bold       and (2^21) or 0)
					+(a.altcharset and (2^22) or 0)
					+(a.invis      and (2^23) or 0)
					+(a.protect    and (2^24) or 0)
					+(a.horizontal and (2^25) or 0)
					+(a.left       and (2^26) or 0)
					+(a.low        and (2^27) or 0)
					+(a.right      and (2^28) or 0)
					+(a.top        and (2^29) or 0)
					+(a.vertical   and (2^30) or 0)
					+(a.italic     and (2^31) or 0))
		else
			error("unsupported type: "..type(a))
		end
		value = bit.bor(value, v)
	end
	return value
end

return {
	_split     = split, -- For internal use.
	_join      = join, -- For internal use.
	NORMAL     = 0x00000000,
	ATTRIBUTES = 0xFFFFFFFFFFFFFF00,
	CHARTEXT   = 0x000000FF,
	COLOR      = 0x0000FF00,
	STANDOUT   = 0x00010000,
	UNDERLINE  = 0x00020000,
	REVERSE    = 0x00040000,
	BLINK      = 0x00080000,
	DIM        = 0x00100000,
	BOLD       = 0x00200000,
	ALTCHARSET = 0x00400000,
	INVIS      = 0x00800000,
	PROTECT    = 0x01000000,
	HORIZONTAL = 0x02000000,
	LEFT       = 0x04000000,
	LOW        = 0x08000000,
	RIGHT      = 0x10000000,
	TOP        = 0x20000000,
	VERTICAL   = 0x40000000,
	ITALIC     = 0x80000000,
}
