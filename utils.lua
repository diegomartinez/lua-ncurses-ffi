
---
-- @module ncurses.utils

local M = { }

---
-- @tparam string sep
-- @tparam function func `func(x:any):string`
-- @tparam any ...
-- @treturn string
function M.sfconcat(sep, func, ...)
	local n, t = select("#", ...), { ... }
	for i = 1, n do
		t[i] = func(t[i])
	end
	return table.concat(t, sep)
end

---
-- @tparam string sep
-- @tparam any ...
-- @treturn string
function M.sconcat(sep, ...)
	return M.sfconcat(sep, tostring, ...)
end

---
-- @tparam any ...
-- @treturn string
function M.concat(...)
	return M.sconcat("", ...)
end

return M
