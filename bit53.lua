
---
-- Bit-op library using Lua 5.3 native bitwise operators.
--
-- Only the subset of functions required by `ncurses` modules are defined.
--
-- @module ncurses.bit53

local M = { }

---
-- Bitwise OR.
-- @tparam number ...
-- @treturn number
function M.bor(...)
	local x, y = ...
	if not y then return x end
	x = x | y
	return M.bor(x, ...)
end

---
-- Bitwise AND.
-- @tparam number ...
-- @treturn number
function M.band(...)
	local x, y = ...
	if not y then return x end
	x = x & y
	return M.band(x, ...)
end

---
-- Bitwise exclusive OR.
-- @tparam number ...
-- @treturn number
function M.bxor(...)
	local x, y = ...
	if not y then return x end
	x = x ~ y
	return M.bxor(x, ...)
end

return M
